<?php
/**
 * @file
 * Returns the HTML for a block. Prevents menu title from printing if title value is <none>.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728246
 */
?>
<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <div class="disable-menu-alert" role="group" aria-labelledby="a11y-notice" aria-live="polite" tabindex="0">
    <div class="image">
      <div class="menu-enabled" tabindex="-1">
        <h2 id="a11y-notice"><span class="material-icons" aria-hidden="true">accessibility</span> Accessibility Notice</h2>
        <p><strong>The following expandable menu may be inaccessible to some users.</strong></p>
        <p>Users can disable the expandable menu and display all menu links if preferred.</p>
        <p class="learn-more-link"><a href="https://designsystem.wwu.edu/notice-drupal-7-menu/">Learn more about this notice.</a></p>
        <button class="menu-disable-button">Disable expandable menu</button>
    </div>
    </div>
  </div>

  <div class="enable-menu-alert" role="status" aria-live="polite" tabindex="0">
    <p class="menu-disabled" tabindex="-1">The expandable menu is currently disabled.</p>
    <p class="learn-more-link"><a href="https://designsystem.wwu.edu/notice-drupal-7-menu/">Learn more about this notice.</a></p>
    <button class="enable-accordion">Enable expandable menu</button>
  </div>

  <?php print render($title_prefix); ?>
  <?php if (($title) != '<none>'): ?>
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php print $content; ?>

</div>
