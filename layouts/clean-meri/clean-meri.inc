<?php

/**
 * @file
 * Plugin definition.
 */

$plugin = array(
  'title' => t('Clean Meri'),
  'category' => t('Western'),
  'icon' => 'clean-meri.png',
  'theme' => 'clean-meri',
  'css' => '../../css/panel-layouts/clean-meri.css',
  'regions' => array(
    'row1' => t('100% First Row'),
    'row2' => t('100% Second Row'),
    'row3left' => t('40% Third Row Left side'),
    'row3right' => t('60% Third Row Right side'),
    'row4left' => t('60% Fourth Row Left side'),
    'row4right' => t('40% Fourth Row Right side'),
    'bottom' => t('100% Bottom')
  ),
);
