/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an anonymous closure. See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

  /**This is to stop Chrome from flashing styles to the screen on page load
   * http://www.learningjquery.com/2008/10/1-way-to-avoid-the-flash-of-unstyled-content/
   * The following code runs after the body loads which then allows the menu to appear.
   */
  Drupal.behaviors.noTransitionFlash = {
    attach: function () {
      $('html').removeClass('preload');
    }
  };

  // START SITE NAME TYPOGRAPHY
  Drupal.behaviors.siteNameTypography = {
    attach: function (context) {
      if (context == document) {
        var siteName = $(".site-name p").text();
        var typographyAnd = siteName.replace("and", "<span class=\"diminutive-type\">and</span>");
        var typographyCollegeOf = typographyAnd.replace("College of", "<span class=\"diminutive-type\">College of</span>");
        var typographyDepartmentOf = typographyAnd.replace("Department of", "<span class=\"diminutive-type\">Department of</span>");
        var typographyDivisionOf = typographyAnd.replace("Division of", "<span class=\"diminutive-type\">Division of</span>");
        var typographyOfficeOf = typographyAnd.replace("Office of", "<span class=\"diminutive-type\">Office of</span>");

        $('.site-name p span:contains(College of)').replaceWith(typographyCollegeOf);
        $('.site-name p span:contains(Department of)').replaceWith(typographyDepartmentOf);
        $('.site-name p span:contains(Division of)').replaceWith(typographyDivisionOf);
        $('.site-name p span:contains(Office of)').replaceWith(typographyOfficeOf);
      }
    }
  };
  // END SITE NAME TYPOGRAPHY

  // START SEARCH TOGGLE ON CLICK
  Drupal.behaviors.searchSlider = {
    attach: function (context) {
      $('.western-search > button', context).click(function () {
        var search = $('.western-search-widget');
        var searchInput = $('.western-search-widget').find('form:first').find('input:first');
        var $search_button = $(this);

        if (search.is(':visible')) {
          search.hide("slide", { direction: "right" }, 400);
          $search_button.attr('aria-label', 'Open the search Box');
        } else {
          search.show("slide", { direction: "right" }, 200);
          searchInput.focus();
          $search_button.attr('aria-label', 'Close the search Box');
        }
      });
    }
  };
  // END SEARCH TOGGLE ON CLICK

  // START QUICK LINKS TOGGLE ON CLICK
  Drupal.behaviors.mobileWwuMenu = {
    attach: function (context) {
      $('.western-quick-links button', context).click(function () {
        var $quicklinks = $('.western-quick-links ul');
        var $quicklinks_button = $(this);

        if ($quicklinks.is(':visible')) {
          $quicklinks.hide("slide", { direction: "right" }, 400);
          $quicklinks_button.attr('aria-label', 'Open Western quick links');
        } else {
            $quicklinks.show("slide", { direction: "right" }, 200, function() {
            $quicklinks.css({
              'display': 'table'
            });
            $quicklinks_button.attr('aria-label', 'Close Western quick links');
          });
        }
      });
    }
  };
  // END QUICK LINKS TOGGLE ON CLICK

  // START MOBILE MAIN MENU TOGGLE ON CLICK
  Drupal.behaviors.mobileWwuMainMenu = {
    attach: function (context) {
      var $mainMenu,
          $mobileMainNav,
          $navRegion,
          $subMenus;

      $mobileMainNav = $('.mobile-main-nav', context);
      $navRegion = $('#main-menu');
      $mainMenu = $('#main-menu div > ul.menu');
      $subMenus = $mainMenu.find('ul');
      var $window = $(window, context);

      if ($window.width() < 800) {
        $navRegion.hide();
      }

      $mobileMainNav.click(function () {
        if ($mainMenu.is(':visible')) {
          $mainMenu.css('display', 'block');
          $mainMenu.find('.opened').removeClass('opened');
          $mainMenu.slideUp({
            complete: function() {
              $navRegion.hide();
            }
          });
          $subMenus.slideUp();
          $mobileMainNav.attr('aria-label', 'Open main navigation');
        } else {
          $navRegion.show();
          $mainMenu.slideDown({
            complete: function () {
              $mainMenu.css('display', 'table');
            }
          });
          $mobileMainNav.attr('aria-label', 'Close main navigation');
        }
      });
    }
  };
  //END MOBILE MAIN MENU TOGGLE ON CLICK

  // START MENU EXPANSION ON CLICK
  Drupal.behaviors.menuExpansion = {
    attach: function (context) {
      var $menu_items = $('#main-menu div > ul.menu li', context);
      var $menu_links = $menu_items.find('a');
      var $submenu_parents = $menu_items.has('ul');
      var $window = $(window, context);

      function closeSubmenu($parent) {
        $parent.removeClass('opened');
        $parent.find('.opened').removeClass('opened');
        $parent.find('ul').slideUp();
      }

      function openSubmenu($parent) {
        $parent.addClass('opened');
        $parent.children('ul').slideDown();
      }

      function clickSubmenuParent() {
        var $this = $(this);

        if ($window.width() > 800) {
          return true;
        }

        if ($this.hasClass('opened')) {
          closeSubmenu($this);
        } else {
          closeSubmenu($this.siblings('.opened'));
          openSubmenu($this);
        }
      }

      $menu_items.click(function (event) {
        if ($window.width() > 800) {
          return true;
        }

        event.stopPropagation();
      });

      $menu_links.click(function (event) {
        if ($window.width() > 800) {
          return true;
        }

        event.stopPropagation();
      });

      $submenu_parents.click(clickSubmenuParent);
    }
  };
  // END MENU EXPANSION ON CLICK

  // START MOBILE MAIN MENU AND QUICK LINKS TOGGLE ON RESIZE
  Drupal.behaviors.toggleMenuOnResize = {
    attach: function (context) {
      var $mainMenu,
          $subMenus,
          $quicklinks,
          $navRegion,
          $window;

      function bindHandlers() {
        $window.unbind('resize', resizeWindow);

        if ($window.width() <= 801) {
          $window.resize(resizeWindow);
        }
      }

      function resizeWindow() {
        if ($window.width() > 800) {
          $mainMenu.removeAttr('style');
          $subMenus.removeAttr('style');
          $navRegion.show();
          $mainMenu.find('.opened').removeClass('opened');
          $quicklinks.css('display','table');
        } else {
          $navRegion.hide();
          $quicklinks.css('display','none');
        }
      }

      $mainMenu = $('#main-menu div > ul.menu');
      $subMenus = $mainMenu.find('ul');
      $quicklinks = $('.western-quick-links ul');
      $navRegion = $('#main-menu');
      $window = $(window, context);
      $window.resize(bindHandlers);
      bindHandlers();
    }
  };
  // END MOBILE MAIN MENU AND QUICK LINKS TOGGLE ON RESIZE

  // START Tooltips for the staff and faculty directories
  Drupal.behaviors.userModuleIcons = {
    attach: function (context) {
      $('.user-module div', context).click(function(){
        if($(this).hasClass('tooltip')){
          $(this).removeClass('tooltip');
        } else {
          $(this).addClass('tooltip').siblings().removeClass('tooltip');
        }
      });
    }
  };
  // END Tooltips for the staff and faculty directories

  // START Slideshow image links
  Drupal.behaviors.slideshowImageLinks = {
    attach: function () {
      $('.slides li').each(function () {
        var $this,
            $link;

        $this = $(this);
        $link = $this.find('.flex-caption a');

        if ($link.length > 0) {
          $this.find('img')
            .wrap($('<a/>', {
              'href': $link.attr('href')
            }))
            .attr('title', $link.text());
        }
      });
    }
  };
  //END Slideshow image links

  // START Accordion menu no-children fix
  Drupal.behaviors.accordionMenuFix = {
    attach: function (context) {
      $('.accordion-menu-wrapper', context).on('accordionbeforeactivate', function (event, ui) {
        if ($(ui.newHeader).hasClass('no-children')) {
          return false;
        }
      });
    }
  };
  // END Accordion menu no-children fix

  //START accordion menu accessibility workaround
  Drupal.behaviors.accordionAccessibility = {
    attach: function (context) {
      function accordionMenuConvert($root) {
        var $ul = $root.children('ul').clone(false);

        if (!$ul.length) {
          $ul = $('<ul>', { 'class': 'menu' });

          $root.children('h3').each(function (index, element) {
            var $h3 = $(element);
            var $div = $h3.next('div');
            var $submenu = accordionMenuConvert($div);
            var $li = $('<li>', { 'class': 'menu__item' });
            var $a = $h3.children('.accordion-link').clone(false);

            if ($a.length) {
              $li.html($a.removeClass('accordion-link').addClass('menu__link'));
            }

            if ($submenu.children().length) {
              $li.append($submenu);
            }
            else {
              $li.addClass('is-leaf').addClass('leaf');
            }

            $ul.append($li);
          });

          $ul.children().first().addClass('first');
          $ul.children().last().addClass('last');
        }

        return $ul;
      }

      $('[class^="accordion-menu-"]', context).each(function () {
        var $accordion = $(this);

        var $enableAlert = $accordion.siblings('.enable-menu-alert');
        var $disableAlert = $accordion.siblings('.disable-menu-alert');

        var $enableAlertButton = $enableAlert.find('.enable-accordion');
        var $disableAlertButton = $disableAlert.find('.menu-disable-button');

        var $menuWarning = $disableAlert.find('.menu-enabled');
        var $menuDisabled = $enableAlert.find('.menu-disabled');

        var $accessibleMenu = accordionMenuConvert($accordion);

        function accordionMenuDisable() {
          $disableAlert.hide();
          $accordion.hide();
          $accordion.addClass('accordion-menu-disabled');
          $accordion.removeClass('accordion-menu-enabled');
          $accordion.children('a').attr('tabindex', '-1');
          $accessibleMenu.show();
          $enableAlert.show();
        }

        function accordionMenuEnable() {
          $enableAlert.hide();
          $accordion.show();
          $accordion.addClass('accordion-menu-enabled');
          $accordion.removeClass('accordion-menu-disabled');
          $accordion.children('a').removeAttr('tabindex');
          $accessibleMenu.hide();
          $disableAlert.show();
        }

        function checkLocalStorage() {
          if (localStorage.getItem('accordiondisabled') === 'true') {
            accordionMenuDisable();
          }
          else {
            accordionMenuEnable();
          }
        }

        function navWrapper () {
          var $accordionDisabled = $('.accordion-menu-disabled');
          var $accessibleMenu = $accordionDisabled.siblings('ul', { 'class': 'menu' });

          if ($accordionDisabled.length) {
            $accessibleMenu.wrapAll("<nav aria-label='Submenu'></nav>");
          }
          else {
            return false;
          }
        }

        $accordion.after($accessibleMenu.hide());
        checkLocalStorage();

        $disableAlertButton.click(function() {
          localStorage.setItem('accordiondisabled', 'true');
          accordionMenuDisable();
          navWrapper();
          $menuDisabled.focus();
          ga('send', 'event', { eventCategory: 'Accessibility', eventAction: 'Enable/Disable', eventLabel: 'Interior Menu Fix'});
        });

        $enableAlertButton.click(function() {
          localStorage.setItem('accordiondisabled', 'false');
          accordionMenuEnable();
          navWrapper();
          $menuWarning.focus();
        });
      });
    }
  };
  // END accordion menu accessibility workaround

  // START accessible text for a[target="blank", target="_blank"]
  Drupal.behaviors.externalLink = {
    attach: function (context) {
      var $linkExternal = $('a[target="_blank"], a[target="blank"]', context);
      var $openIcon = '<span class="material-icons" aria-hidden="true">open_in_new</span>';
      var $openText = '<span class="visually-hidden">(opens in new window)</span>';

      $linkExternal.append($openIcon);
      $linkExternal.append($openText);
      $linkExternal.attr('rel', 'noopener noreferrer');
    }
  }
  // END accessible text for a[target="blank", target="_blank"]
  
  //Wrap all iframes in a div so that our sass can scale them correctly.
  $("iframe").each(function(){
     $(this).wrap("<div style='--aspect-ratio:16/9;'></div>");
  });

})(jQuery, Drupal, this, this.document);
